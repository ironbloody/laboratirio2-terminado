# !/usr/bin/env python3
# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

from dlgPrincipal import dlgPrincipal

from dlgAdvertencia import dlgAdvertencia

from dlgEliminar import dlgEliminar

from dlgAnadir import dlgAnadir


class wnPrincipal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("/home/v/Descargas/Laboratorio2/ejemplo.ui")

        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Ventana Ejemplo")
        self.window.resize(600, 400)
        self.window.show_all()

        # botones de menú
        boton_anadir = self.builder.get_object("btnAnadir")
        boton_anadir.connect("clicked", self.boton_anadir_clicked)
        boton_editar = self.builder.get_object("btnEditar")
        boton_editar.connect("clicked", self.boton_editar_clicked)
        boton_eliminar = self.builder.get_object("btnEliminar")
        boton_eliminar.connect("clicked", self.boton_eliminar_clicked)

        # label
        self.label_changed = self.builder.get_object("labelChanged")

        # liststore
        self.liststore = self.builder.get_object("treeEjemplo")
        self.liststore.connect("cursor-changed", self.liststore_changed)
        # self.model = Gtk.ListStore(str,
        self.model = Gtk.ListStore(*(2 * [str]))
        self.liststore.set_model(model=self.model)

        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Nombre",
                                    cell_renderer=cell,
                                    text=0)

        self.liststore.append_column(column)

        cell_edit = Gtk.CellRendererText()
        cell_edit.set_property("editable", True)
        cell_edit.set_property("scale", 1.0)
        cell_edit.connect("edited", self.cell_text_apellido_edited)

        column = Gtk.TreeViewColumn(title="Apellido",
                                    cell_renderer=cell_edit,
                                    text=1)
        cell_edit = Gtk.CellRendererText()
        cell_edit.set_property("editable", True)
        cell_edit.set_property("scale", 1.0)
        cell_edit.connect("edited", self.cell_text_apellido_edited)

        column = Gtk.TreeViewColumn(title="Apellido",
                                    cell_renderer=cell_edit,
                                    text=1)
        self.liststore.append_column(column)

        self.model.append(["Fabio", "Durán"])
        self.model.append(["Pepito", "Pepón"])

    def liststore_changed(self, tree=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        nombre = model.get_value(it, 0)
        nombre = ''.join(["Nombre: ",
                          nombre])
        apellido = model.get_value(it, 1)
        self.label_changed.set_text(nombre + " " + apellido)

    def cell_text_apellido_edited(self, cell_renderer, path, text):
        self.model[path][1] = text
        # Si el texto es vacio, soltara una advertencia
        if text == "":
            advertencia = dlgAdvertencia()

    def boton_anadir_clicked(self, btn=None):
        dlg = dlgPrincipal(titulo="Añadir persona")
        response = dlg.dialogo.run()
        if response == Gtk.ResponseType.OK:
            nombre = dlg.nombre.get_text()
            apellido = dlg.apellido.get_text()
            """ Cuando el nombre o el apellido esten vacios,
            el programa soltara una advertencia"""
            if nombre == "" or apellido == "":
                advertencia = dlgAdvertencia()
                dlg.dialogo.destroy()

            else:
                self.model.append([nombre, apellido])
                dlg.dialogo.destroy()
                anadido = dlgAnadir()

    def boton_editar_clicked(self, btn=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        dlg = dlgPrincipal()
        dlg.nombre.set_text(model.get_value(it, 0))
        dlg.apellido.set_text(model.get_value(it, 1))
        response = dlg.dialogo.run()
        if response == Gtk.ResponseType.OK:
            """ Cuando el nombre o el apellido esten vacios,
            el programa soltara una advertencia"""
            if dlg.nombre.get_text() == "" or dlg.apellido.get_text() == "":
                advertencia = dlgAdvertencia()
                dlg.dialogo.destroy()
            else:
                model.set_value(it, 0, dlg.nombre.get_text())
                model.set_value(it, 1, dlg.apellido.get_text())
                dlg.dialogo.destroy()

    def boton_eliminar_clicked(self, btn=None):
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return
        # se llama a la ventana eliminar
        delete = dlgEliminar()
        response = delete.eliminar.run()
        # Cuando la respuesta sea OK se eliminara la celda
        if response == Gtk.ResponseType.OK:
            model.remove(it)
        if response == Gtk.ResponseType.CANCEL:
            pass


if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()

