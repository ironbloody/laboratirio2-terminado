#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgEliminar():
    def __init__(self, titulo=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("/home/v/Descargas/Laboratorio2/ejemplo.ui")

        self.eliminar = self.builder.get_object("dlgEliminar")
        self.eliminar.set_title(titulo)
        self.eliminar.resize(600, 400)
        self.eliminar.show_all()

        # boton aceptar
        boton_aceptar = self.eliminar.add_button(Gtk.STOCK_OK,
                                                 Gtk.ResponseType.OK)
        boton_aceptar.set_always_show_image(True)
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)

        # boton cancelar
        boton_cancelar = self.eliminar.add_button(Gtk.STOCK_CANCEL,
                                                  Gtk.ResponseType.CANCEL)
        boton_cancelar.set_always_show_image(True)
        boton_cancelar.connect("clicked", self.boton_cancelar_clicked)

    def boton_aceptar_clicked(self, btn=None):
        self.eliminar.destroy()

    def boton_cancelar_clicked(self, btn=None):
        self.eliminar.destroy()

